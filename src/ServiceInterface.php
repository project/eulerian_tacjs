<?php

declare(strict_types=1);

namespace Drupal\eulerian_tacjs;

/**
 * Declare Eulerian TacJS service properties.
 */
interface ServiceInterface {

  /**
   * Service name.
   *
   * @var string
   */
  const NAME = 'eulerian-analytics';

  /**
   * Service type.
   *
   * @var string
   */
  const TYPE = 'analytic';

}
