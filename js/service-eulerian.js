((drupalSettings, tarteaucitron, window) => {
  "use strict";

  drupalSettings.eulerian = drupalSettings.eulerian || {};

  tarteaucitron.services["eulerian-analytics"] = {
    "key": "eulerian-analytics",
    "type": "analytic",
    "name": "Eulerian Analytics",
    "needConsent": true,
    "cookies": ["etuix"],
    "uri" : "https://www.eulerian.com/rgpd",
    "js": () => {
      if (typeof drupalSettings.eulerian.domain == "undefined") {
        return;
      }

      /* eslint-disable */
      (function (x,w) { if (!x._ld){ x._ld = 1;
        let ff = function () { if(x._f){x._f('tac',tarteaucitron,1)} };
        w.__eaGenericCmpApi = function (f) { x._f = f; ff(); };
        w.addEventListener("tac.close_alert", ff);
        w.addEventListener("tac.close_panel", ff);
      }})(this, window);
      /* eslint-enable */
    },
    "fallback": () => {
      tarteaucitron.services["eulerian-analytics"].js();
    }
  };

})(drupalSettings, tarteaucitron, window);
